﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Sorting_alghoritms
{
    public class CountingSort : ISortable
    {
        private string path;
        public bool isSorting;
        public List<int> Pole { get; set; }
        public string Name { get; set; }

        public string Path
        {
            get { return this.path; }
            set { this.path = value; }
        }

        public CountingSort(string path)
        {
            this.path = path;
            this.isSorting = false;
            this.Name = "Countingsort";
        }
        
        public async Task<List<int>> Sort()
        {
            isSorting = true;
            int[] serazenePole = new int[Pole.Count];
            
            //Zjištění četností hodnot klíčů ve vstupním poli
            await Task.Run(() => {
                int max = Pole.Max();
                int min = Pole.Min();
                int[] pocetC = new int[max - min + 1];
                
                
                for (int i = 0; i < Pole.Count; i++)
            {
                pocetC[Pole[i] - min]++;
            }

            //Přepočítání pole četnosti C
            pocetC[0]--;
            for (int i = 1; i < pocetC.Length; i++)
            {
                pocetC[i] = pocetC[i - 1] + pocetC[i];
            }

            //Umístění klíčů do výstupního pole 'serazenePole' na správné místo
            
            for (int i = Pole.Count - 1; i >= 0; i--)
            {

                serazenePole[pocetC[Pole[i] - min]] = Pole[i];
                pocetC[Pole[i] - min]--;
            }
                Pole = serazenePole.ToList();
            });
            
            

            isSorting = false;
            return Pole;
        }

        public async Task Start(MainWindow window)
        {
            Stopwatch stopWatch = new Stopwatch();
            window.countingButton.Content = "Třídím...";
            Pole = FileSlave.Read(path);
            stopWatch.Start();
            List<int> result = await Sort();
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0} ms.", ts.Milliseconds);
            window.countingTimer.Content = elapsedTime;
            window.countingButton.Content = "Dokončeno";
            FileSlave.Write(path + "\\countingsort.txt", result);
            MessageBox.Show("Zapsání Countingsortu dokončeno");
        }

    }
}
