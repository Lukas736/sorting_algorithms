﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting_alghoritms
{
    public class GrafViewModel
    {
        public PlotModel GrafModel { get; private set; }
        public IList<DataPoint> DataPoints { get; set; }

        private Dictionary<string, LineSeries> series = new Dictionary<string, LineSeries>();

        private BubbleSort bubble = new BubbleSort(null);
        private CountingSort counting = new CountingSort(null);
        private HeapSort heap = new HeapSort(null);
        public GrafViewModel()
        {
            this.GrafModel = new PlotModel { Title = "Grafy" };
            //this.GrafModel.Series.Add(new FunctionSeries(Math.Cos, 0, 10, 0.1, "cos(x)"));
            //MakeFilesForSorting(-10000, 10000);
            //AddSerie(bubble, -10000, 10000);
            //AddSerie(counting, -10000, 10000);
            //AddSerie(heap, -10000, 10000);
            //foreach (string key in series.Keys)
            //{
            //    this.GrafModel.Series.Add(series[key]);
            //}

            //serie.Points.Add(new DataPoint(0, 5));
            //this.GrafModel.Series.Add(serie);

        }

        private void AddSerie(ISortable sortalg, int min, int max)
        {
            Stopwatch stopwatch = new Stopwatch();
            LineSeries serie = new LineSeries
            {
                Title = sortalg.Name
            };
            
            for (int i = 10000; i <= 100000; i += 10000)
            {
                stopwatch.Reset();
                sortalg.Pole = Generator.Generuj(i, max, min);
                stopwatch.Start();
                sortalg.Sort();
                stopwatch.Stop();
                serie.Points.Add(new DataPoint(i, stopwatch.Elapsed.TotalMilliseconds));
            }
            
            series.Add(sortalg.Name, serie);
        }

        //private void MakeFilesForSorting(int min, int max)
        //{
        //    for (int i = 10000; i <= 100000; i += 10000)
        //    {
        //       FileSlave.Write(FileSlave.DefaultPath + "\\graphs\\" + i.ToString() + ".txt", Generator.Generuj(i, max, min));
        //    }
            
        //}

        private void Run()
        {

        }
    }
}
