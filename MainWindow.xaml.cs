﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using OxyPlot;
using OxyPlot.Wpf;

namespace Sorting_alghoritms
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
            this.bubblesort = new BubbleSort(generatorWindow.generatorPathTextBox.Text);
            this.countingsort = new CountingSort(generatorWindow.generatorPathTextBox.Text);
            this.heapsort = new HeapSort(generatorWindow.generatorPathTextBox.Text);
        }
        // Deklarace proměnných

        public GeneratorWindow generatorWindow = new GeneratorWindow();
        public BubbleSort bubblesort;
        public CountingSort countingsort;
        public HeapSort heapsort;
        public GrafWindow grafWindow = new GrafWindow();


        private async void bubblesort_Click(object sender, RoutedEventArgs e)
        {
            if (bubblesort.isSorting)
            {
                MessageBox.Show("Bubblesort ještě třídí!");
                
            }
            else
            {
                await bubblesort.Start(this);
            }
                  
            
        }


        private async void countingsort_Click(object sender, RoutedEventArgs e)
        {
            if (countingsort.isSorting)
            {
                MessageBox.Show("Countingsort ještě třídí!");
                
            }
            else
            {
                await countingsort.Start(this);
            }
           
        }

        private async void heapsort_Click(object sender, RoutedEventArgs e)
        {
            if (heapsort.isSorting)
            {
                MessageBox.Show("Heapsort ještě třídí!");
            }
            else
            {
                await heapsort.Start(this);
            }
            
        }


        private void generator_Click(object sender, RoutedEventArgs e)
        {
            generatorWindow.Show();
        }

        

        private void mainWindow_Closed(object sender, EventArgs e)
        {
            App.Current.Shutdown();
        }

        private async void runAllButton_Click(object sender, RoutedEventArgs e)
        {           
            if (bubblesort.isSorting || countingsort.isSorting || heapsort.isSorting)
            {
                MessageBox.Show("Jeden z algoritmů ještě třídí!");
            }
            else
            {
                await Task.WhenAll(bubblesort.Start(this), countingsort.Start(this), heapsort.Start(this));
                MessageBox.Show("Vše je hotové!");
            }
            
        }

        private void graf_Click(object sender, RoutedEventArgs e)
        {
            
            grafWindow.Show();
        }
    }
}
