﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sorting_alghoritms
{
    interface ISortable
    {
        string Name { get; set; }
        List<int> Pole { get; set; }
        Task<List<int>> Sort();


    }
}
