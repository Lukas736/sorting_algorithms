﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Diagnostics;

namespace Sorting_alghoritms
{
    public class BubbleSort :ISortable
    {
        private string path;
        public bool isSorting;
        public List<int> Pole { get; set; }
        public string Name { get; set; }

        public BubbleSort(string path)
        {
            this.path = path;
            this.isSorting = false;
            this.Name = "Bubblesort";
        }

        public async Task<List<int>> Sort()
        {
            int i = 0;
            int prohozeni = 1;
            isSorting = true;
            await Task.Run(() =>
            {
                while ((i < Pole.Count - 1) && (prohozeni > 0))
                {
                    prohozeni = 0;
                    for (int j = 0; j < Pole.Count - 1 - i; j++)
                    {
                        if (Pole[j] > Pole[j + 1])
                        {
                            int pom = Pole[j];
                            Pole[j] = Pole[j + 1];
                            Pole[j + 1] = pom;
                            prohozeni++;

                        }

                    }
                    i++;
                }

            });
            isSorting = false;
            return Pole;

        }


        public async Task Start(MainWindow window)
        {
            Stopwatch stopWatch = new Stopwatch();      //#2        
            window.bubblesortButton.Content = "Třídím...";          //#1
            stopWatch.Start(); //#2
            Pole = FileSlave.Read(path);
            List<int> result = await Sort();                        //#1
            stopWatch.Stop(); //#2
            TimeSpan ts = stopWatch.Elapsed; //#2
            string elapsedTime; //#2
            if (ts.Minutes > 0)
            {
                elapsedTime = String.Format("{0:00} minut a {1:00} vteřin.", ts.Minutes, ts.Seconds);
            }
            else
            {
                elapsedTime = String.Format("{0:00} vteřin {1} ms", ts.Seconds, ts.Milliseconds);
            }
            window.bubbleTimer.Content = elapsedTime; //#2
            window.bubblesortButton.Content = "Dokončeno";          //#1
            FileSlave.Write(path + "\\bubblesort.txt", result);    //#1
            MessageBox.Show("Zapsání Bubblesortu dokončeno");       //#1
        }

    }
}
