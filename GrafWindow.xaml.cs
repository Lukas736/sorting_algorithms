﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using OxyPlot;
using OxyPlot.Series;
using System.Diagnostics;
using OxyPlot.Wpf;

namespace Sorting_alghoritms
{
    /// <summary>
    /// Interakční logika pro GrafWindow.xaml
    /// </summary>
    public partial class GrafWindow : Window
    {
        public GrafWindow()
        {   
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Visibility = Visibility.Hidden;
            
        }


    }

}
