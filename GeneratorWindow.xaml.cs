﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Sorting_alghoritms
{
    /// <summary>
    /// Interakční logika pro GeneratorWindow.xaml
    /// </summary>
    public partial class GeneratorWindow : Window
    {
        public GeneratorWindow()
        {
            InitializeComponent();
            
        }

        private void Generator_Click(object sender, RoutedEventArgs e)
        {
            string path = generatorPathTextBox.Text;
            int pocetCisel = Int32.Parse(pocetCiselTextBox.Text);
            int maximum = Int32.Parse(maximumTextBox.Text);
            int minimum = Int32.Parse(minimumTextBox.Text);
            if (maximum < minimum)
            {
                MessageBox.Show("Maximum nemůže být menší než minimum!");
                
            }
            else if (pocetCisel <= 1)
            {
                MessageBox.Show("Počet čísel musí být větší než 1!");
            }
            else
            {
                FileSlave.Write(path + "\\numbers.txt", Generator.Generuj(pocetCisel, maximum, minimum));
                MessageBox.Show("Čísla vygenerována!");

                Visibility = Visibility.Hidden;
            }
            

            

        }

        private void generatorWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Visibility = Visibility.Hidden;
            e.Cancel = true;
        }
    }
}
