﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.IO;

namespace Sorting_alghoritms
{
    public class HeapSort : ISortable
    {
        private string path;
        public bool isSorting;
        public List<int> Pole { get; set; }
        public string Name { get; set; }
        public HeapSort(string path)
        {
            this.path = path;
            this.isSorting = false;
            this.Name = "Heapsort";
        }


        public async Task<List<int>> Sort()
        {
            
            isSorting = true;
            Heapify(Pole); //Vytvoření haldy
            int index = Pole.Count - 1; //Poslední prvek
            int pom;
            await Task.Run(() => { 
            while (index > 0)
            {
                pom = Pole[0]; //Prohození posledního prvku s posledním
                Pole[0] = Pole[index];
                Pole[index] = pom;
                index -= 1; //Nový poslední prvek
                HeapDown(Pole, index);
            }
            });
            isSorting = false;
            return Pole;
        }


        private void HeapUp(List<int> pole, int i)
        {

            int potomek = i; //uložení potomka
            int rodic;
            while (potomek != 0)
            {
                rodic = (potomek - 1) / 2; //rodič
                if (pole[rodic] < pole[potomek])
                {
                    int pom = pole[potomek];  //prohození rodiče s potomkem
                    pole[potomek] = pole[rodic];
                    pole[rodic] = pom;
                    potomek = rodic; //nový potomek
                }
                else
                {
                    return;
                }

            }
        }

        private List<int> HeapDown(List<int> pole, int posledni)
        {
            int rodic = 0;
            int potomek;
            int pom;
            while (rodic * 2 + 1 <= posledni)
            {
                potomek = rodic * 2 + 1;
                //Je vybrán menší potomek
                if ((potomek < posledni) && (pole[potomek] < pole[potomek + 1]))
                {
                    potomek++; //Vybereme toho většího
                }
                if (pole[rodic] < pole[potomek])
                {
                    pom = pole[rodic]; //Prohození rodiče s potomkem
                    pole[rodic] = pole[potomek];
                    pole[potomek] = pom;
                    rodic = potomek; //Nový rodič
                }
                else
                {
                    break;
                }
            }
            return pole;
        }

        public void Heapify(List<int> pole)
        {
            for (int i = 1; i < pole.Count; i++)
            {
                HeapUp(pole, i);
            }
        }

        public async Task Start(MainWindow window)
        {
            Stopwatch stopWatch = new Stopwatch();
            window.heapButton.Content = "Třídím...";
            Pole = FileSlave.Read(path);
            stopWatch.Start();
            List<int> result = await Sort();
            stopWatch.Stop();            
            window.heapButton.Content = "Dokončeno";
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0} ms.",ts.Milliseconds);
            window.heapTimer.Content = elapsedTime;
            FileSlave.Write(path + "\\heapsort.txt",result);
            MessageBox.Show("Zapsání HeapSortu dokončeno");
        }




    }
}
